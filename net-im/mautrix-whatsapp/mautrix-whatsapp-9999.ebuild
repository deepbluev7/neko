# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
EGO_PN="github.com/tulir/mautrix-whatsapp"

inherit bash-completion-r1 go-module

DESCRIPTION="GitHub CLI"
HOMEPAGE="https://github.com/tulir/mautrix-whatsapp.git"

if [[ ${PV} == *9999 ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/tulir/mautrix-whatsapp.git"
else
	EGO_SUM=""
	EGIT_COMMIT="v${PV}"
	KEYWORDS="~amd64 ~x86"
	go-module_set_globals
fi

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="dev-libs/olm"
DEPEND="dev-lang/go
${RDEPEND}"
BDEPEND=""

src_unpack() {
	if [[ ${PV} == *9999 ]]; then
		git-r3_src_unpack
		go-module_live_vendor
	else
		go-module_src_unpack
	fi
}

src_compile() {
	go build
}

src_install() {
	newinitd "${FILESDIR}/matrix-whatsapp.initd" matrix-whatsapp
	dobin mautrix-whatsapp
	dodir /etc/mautrix-whatsapp
	insinto /etc/mautrix-whatsapp
	newins example-config.yaml config.yaml

	dodoc example-config.yaml
}
