# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9,10,11} )
DISTUTILS_USE_SETUPTOOLS=rdepend
inherit distutils-r1

DESCRIPTION="A plugin-based Matrix bot system."
HOMEPAGE="https://github.com/mautrix/telegram"
SRC_URI="https://github.com/mautrix/telegram/archive/v${PV/_rc/-rc}.tar.gz"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

S="${WORKDIR}/telegram-${PV/_rc/-rc}"

RDEPEND=">=dev-python/mautrix-0.17.8[${PYTHON_USEDEP}]
dev-python/aiohttp[${PYTHON_USEDEP}]
dev-python/commonmark[${PYTHON_USEDEP}]
>=dev-python/ruamel-yaml-0.15.35[${PYTHON_USEDEP}]
>=dev-python/python-magic-0.4[${PYTHON_USEDEP}]
>=dev-python/tulir-telethon-1.25.0_alpha20[${PYTHON_USEDEP}]
dev-python/asyncpg[${PYTHON_USEDEP}]
>=dev-python/mako-1[${PYTHON_USEDEP}]
>=dev-python/yarl-1[${PYTHON_USEDEP}]

acct-user/synapse
"
#hq_thumbnails? ( dev-python/moviepy[${PYTHON_USEDEP}] ) dependency removed, since it is removed from the portage repos

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
	test? ( ${RDEPEND}
		dev-python/pytest[${PYTHON_USEDEP}] )"

src_prepare()
{
	rm tests -R
	sed -i 's|(".",|("/etc/mautrix-telegram/",|' setup.py || die "Sed failed"
	distutils-r1_python_prepare_all
}

src_install()
{
	distutils-r1_src_install
	insinto /etc/mautrix-telegram/
	doins mautrix_telegram/example-config.yaml
}
