# Copyright 2019-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Desktop client for the Matrix protocol"
HOMEPAGE="https://github.com/Nheko-Reborn/nheko"

inherit eutils cmake xdg-utils

if [[ ${PV} == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/Nheko-Reborn/${PN}.git"
else
	SRC_URI="https://github.com/Nheko-Reborn/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE="+voip"

RDEPEND=">=dev-qt/qtgui-5.10
		>=dev-qt/qtnetwork-5.10
		>=dev-qt/qtquickcontrols2-5.10
		>=dev-qt/qtgraphicaleffects-5.10
		>=dev-qt/qtdeclarative-5.10
		dev-qt/qtmultimedia[qml,gstreamer]
		dev-qt/qtsvg
		dev-qt/qtconcurrent
		dev-qt/qtwidgets
		dev-qt/qtdbus
		dev-libs/qtkeychain
		media-libs/fontconfig
		app-text/cmark
		dev-db/lmdb++
		voip? ( media-plugins/gst-plugins-libnice )
		voip? ( media-plugins/gst-plugins-srtp )
		voip? ( media-plugins/gst-plugins-webrtc )
		"
DEPEND="${RDEPEND}
		>=dev-qt/linguist-tools-5.10
		dev-util/ninja
		~dev-libs/mtxclient-9999
		dev-libs/spdlog
		"

pkg_postinst() {
	xdg_desktop_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
}
