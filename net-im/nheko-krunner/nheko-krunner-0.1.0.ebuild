# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A KRUnner plugin for Nheko"
HOMEPAGE="https://github.com/LorenDB/nheko-krunner"

KFMIN="5.90.0"

inherit ecm

if [[ ${PV} == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/LorenDB/${PN}.git"
else
	SRC_URI="https://github.com/LorenDB/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-qt/qtgui:5
		dev-qt/qtdbus:5
		>=kde-frameworks/krunner-${KFMIN}:5
		>=kde-frameworks/ki18n-${KFMIN}:5
"
RDEPEND="${DEPEND}"
BDEPEND=""
