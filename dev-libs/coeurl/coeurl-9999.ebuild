# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A simple async wrapper around CURL for C++"
HOMEPAGE="https://nheko.im/nheko-reborn/coeurl"

inherit eutils meson

if [[ ${PV} == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://nheko.im/nheko-reborn/coeurl.git"
else
	SRC_URI="https://nheko.im/nheko-reborn/coeurl/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64 ~x86"
fi

LICENSE="MIT"
SLOT="0"
IUSE="-test"

RDEPEND="
	net-misc/curl
	dev-libs/libevent
	dev-libs/spdlog
	"
DEPEND="${RDEPEND}"

src_configure() {
	local emesonargs=(
		$(meson_use test tests)
	)
	meson_src_configure
}
