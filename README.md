# Personal Gentoo overlay

This is my personal overlay with a few random packages in varying states of maintenance. It was originally forked from PureTryOuts matrix-overlay

### Manually

1. Create a file in `/etc/portage/repos.conf`, i.e.: `/etc/portage/repos.conf/neko.conf`, with the following contents:

```
[neko]
location = /var/db/repos/neko
sync-type = git
sync-uri = https://gitlab.com/deepbluev7/neko.git
```

2. Sync your repos as usually, which should pick up the new overlay automatically

