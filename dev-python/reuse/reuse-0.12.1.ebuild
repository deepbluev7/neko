# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{9..10} )

inherit distutils-r1

DESCRIPTION="The tool for checking and helping with compliance with the REUSE recommendations"

HOMEPAGE="https://reuse.software"
LICENSE="GPL-3+"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P}.tar.gz"

RDEPEND="dev-python/boolean-py[${PYTHON_USEDEP}]
dev-python/binaryornot[${PYTHON_USEDEP}]
dev-python/license-expression[${PYTHON_USEDEP}]
dev-python/python-debian[${PYTHON_USEDEP}]
"
DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"

SLOT="0"
KEYWORDS="~amd64 ~x86"
