# Copyright 2019-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8..10} )
inherit distutils-r1

DESCRIPTION="A plugin-based Matrix bot system."
HOMEPAGE="https://pypi.org/project/maubot/"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P/_rc/.dev}.tar.gz"

LICENSE="AGPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE="test e2be"

S="${WORKDIR}/${P/_rc/.dev}"
PATCHES=( "${FILESDIR}/prevent-cluttering-usr-0.3.patch" )

RDEPEND=">=dev-python/mautrix-0.15.5[${PYTHON_USEDEP}]
dev-python/aiohttp[${PYTHON_USEDEP}]
dev-python/yarl[${PYTHON_USEDEP}]
<=dev-python/sqlalchemy-1.5.0[${PYTHON_USEDEP}]
dev-python/asyncpg[${PYTHON_USEDEP}]
dev-python/aiosqlite[${PYTHON_USEDEP}]
dev-python/commonmark[${PYTHON_USEDEP}]
dev-python/ruamel-yaml[${PYTHON_USEDEP}]
dev-python/attrs[${PYTHON_USEDEP}]
dev-python/bcrypt[${PYTHON_USEDEP}]
dev-python/packaging[${PYTHON_USEDEP}]

dev-python/click[${PYTHON_USEDEP}]
>=dev-python/colorama-0.4[${PYTHON_USEDEP}]
dev-python/questionary[${PYTHON_USEDEP}]
dev-python/jinja[${PYTHON_USEDEP}]

acct-user/maubot
acct-group/synapse

e2be? (
	dev-python/olm[${PYTHON_USEDEP}]
	dev-python/unpaddedbase64[${PYTHON_USEDEP}]
	dev-python/pycryptodome[${PYTHON_USEDEP}]
	)
"
DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
	test? ( ${RDEPEND}
		dev-python/pytest[${PYTHON_USEDEP}] )"

src_install()
{
	distutils-r1_src_install
	insinto /etc/maubot/
	doins maubot/example-config.yaml
}
