# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=(python3_{7,8,9,10})

inherit distutils-r1

DESCRIPTION="Python CFFI binding to libolm"
HOMEPAGE="https://gitlab.matrix.org/matrix-org/olm/"
SRC_URI="https://gitlab.matrix.org/matrix-org/olm/-/archive/${PV}/olm-${PV}.tar.bz2 -> ${P}.tar.bz2"
KEYWORDS="~x86 ~amd64"
SLOT="0"
LICENSE="Apache-2.0"

S="${S}/python"
DOCS=""

CDEPEND="
	>=dev-libs/olm-${PV}
"

DEPEND="${CDEPEND}
dev-python/wheel[${PYTHON_USEDEP}]
"
RDEPEND="${CDEPEND}"
