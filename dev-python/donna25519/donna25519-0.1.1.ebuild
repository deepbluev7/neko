# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8,9,10} )
inherit distutils-r1

DESCRIPTION="Python wrapper for the Curve25519-donna cryptographic library"
HOMEPAGE="https://github.com/Muterra/donna25519 https://pypi.python.org/pypi/donna25519"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P/_rc/.dev}.zip"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
