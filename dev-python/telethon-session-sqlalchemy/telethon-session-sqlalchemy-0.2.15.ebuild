# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{6,7,8,9,10,11} )
inherit distutils-r1

DESCRIPTION="SQLAlchemy backend for Telethon session storage"
HOMEPAGE="https://github.com/tulir/telethon-session-sqlalchemy"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P/_rc/.dev}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

S="${WORKDIR}/${P/_rc/.dev}"

RDEPEND="dev-python/sqlalchemy[${PYTHON_USEDEP}]
dev-python/Telethon[${PYTHON_USEDEP}]
"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
	test? ( ${RDEPEND}
		dev-python/pytest[${PYTHON_USEDEP}] )"
