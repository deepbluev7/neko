# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{8,9,10} )
inherit distutils-r1

DESCRIPTION="Command line admin tool for Synapse (Matrix reference homeserver)"
HOMEPAGE="https://github.com/JOJ0/synadm"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P/_rc/.dev}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND="
dev-python/click[${PYTHON_USEDEP}]
dev-python/requests[${PYTHON_USEDEP}]
dev-python/pyyaml[${PYTHON_USEDEP}]
dev-python/tabulate[${PYTHON_USEDEP}]
dev-python/click-option-group[${PYTHON_USEDEP}]
"
DEPEND="${RDEPEND}"
