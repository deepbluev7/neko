# Copyright 2019-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..11} )
inherit distutils-r1

DESCRIPTION="A Python 3.6+ asyncio Matrix framework."
HOMEPAGE="https://pypi.org/project/mautrix/"
SRC_URI="mirror://pypi/${PN:0:1}/${PN}/${P/_rc/.dev}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"

S="${WORKDIR}/${P/_rc/.dev}"

RDEPEND="dev-python/aiohttp[${PYTHON_USEDEP}]
dev-python/attrs[${PYTHON_USEDEP}]
dev-python/yarl[${PYTHON_USEDEP}]
"

DEPEND="
	test? ( ${RDEPEND}
		dev-python/pytest[${PYTHON_USEDEP}] )"
