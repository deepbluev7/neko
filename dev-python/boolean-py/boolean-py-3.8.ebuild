# Copyright 2021-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{7..10} )

inherit distutils-r1

DESCRIPTION="This library helps you deal with boolean expressions."

HOMEPAGE="https://pypi.org/project/boolean-py/"
LICENSE="BSD-2"
SRC_URI="mirror://pypi/${PN:0:1}/boolean.py/boolean.py-${PV}.tar.gz"

S="${WORKDIR}/boolean.py-${PV}"

SLOT="0"
KEYWORDS="~amd64 ~x86"
